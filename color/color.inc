<?php
$info = array(
  'fields' => array(
    'base'            => t('Base'),
    'background'      => t('Background'),
    'text'            => t('Text'),
    'link'            => t('Link'),
    'linkhover'       => t('Hovered Link'),
    'linkunderline'   => t('Link underline'),
    'borders'         => t('Borders'),
  ),
  'schemes' => array(
      'default' => array(
      'title' => t('Paper (Default)'),
      'colors' => array(
        'base'            => '#ffffff',
        'background'      => '#ffe9cc',
        'text'            => '#333333',
        'link'            => '#0090ff',
        'linkhover'       => '#ff9200',
        'borders'         => '#434c52',
      ),
    ),
    'Oldschool' => array(
      'title' => t('Oldschool'),
      'colors' => array(
        'base'            => '#ffffff',
        'background'      => '#ffffff',
        'text'            => '#333333',
        'link'            => '#0090ff',
        'linkhover'       => '#ff9200',
        'borders'         => '#434c52',
      ),
    ),

  ),
  'css' => array(
    'css/styles.css',
  ),
  'copy' => array(
    'logo.png',
    'images/druplicon.png',
  ),
  'gradients' => array(
    array(
      'dimension' => array(0, 0, 760, 100),
      'direction' => 'vertical',
      'colors' => array('text', 'link'),
    ),
  ),
  'fill' => array(
    'background' => array(0, 0, 760, 321),
    'borders' => array(100, 250, 476, 42),
    ),
  'slices' => array(
    'images/header.png' => array(40, 0, 40, 125),
    'images/menu-item-active-left.png' => array(100, 250, 428, 42),
    'images/menu-item-active-right.png' => array(530, 250, 47, 42),
  ),
  'blend_target' => '#ffffff',
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',
  'base_image' => 'color/base.png',
  'preview_html' => 'color/preview.html',
  'preview_js' => 'color/preview.js',


);
