<?php
/**
 * Override or insert vars into the html template.
 */
function pushtheme_process_html(&$vars) {
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

