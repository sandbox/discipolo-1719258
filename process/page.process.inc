<?php

/**
 * @file
 * process and alter hooks for the
 * pushtheme theme.
 */
 
 

function pushtheme_process_page(&$vars) {
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}


