<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * pushtheme theme.
 */
 
/**
 * Preprocessor for theme('page').
 */
function pushtheme_preprocess_page(&$vars) {
 
 // Overlay is enabled.
  $vars['overlay'] = (module_exists('overlay') && overlay_get_mode() === 'child');
  // Toolbar is enabled
  $vars['toolbar'] = module_exists('toolbar');

  // local tasks
  $vars['primary_local_tasks'] = $vars['secondary_local_tasks'] = FALSE;
  if (!empty($vars['tabs']['#primary'])) {
    $vars['primary_local_tasks'] = $vars['tabs'];
    unset($vars['primary_local_tasks']['#secondary']);
  }
  if (!empty($vars['tabs']['#secondary'])) {
    $vars['secondary_local_tasks'] = array(
      '#theme' => 'menu_local_tasks',
      '#secondary' => $vars['tabs']['#secondary'],
    );
  }
}
  
  
#/**
# * Implements theme_menu_local_tasks().
# */
#function pushtheme_extension_topbar_menu_local_tasks(&$vars) {
#  $output = '';
#  if (!empty($vars['primary'])) {
#    $vars['primary']['#prefix'] = '<ul class="nav nav-tabs">';
#    $vars['primary']['#suffix'] = '</ul>';
#    $output .= drupal_render($vars['primary']);
#  }
#  if (!empty($vars['secondary'])) {
#    $vars['secondary']['#prefix'] = '<ul class="nav nav-pills">';
#    $vars['secondary']['#suffix'] = '</ul>';
#    $output .= drupal_render($vars['secondary']);
#  }
#  return $output;
#}
