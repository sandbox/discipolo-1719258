<?php

/**
 * @file
 * Contains the theme settings form elements for the scripts extension.
 */

/**
 * Implements hook_extension_EXTENSION_theme_settings_form_alter().
 */
function pushtheme_extension_topbar_settings_form($element, &$form, $form_state) {

// Container 
  $element['topbar'] = array(
    '#type' => 'container',
    '#title' => t('topbar'),
  );

    $element['topbar']['use_topbar'] = array(
      '#type' => 'checkbox', 
      '#title' => t('Use the taskbar'), 
      '#default_value' => omega_theme_get_setting('use_topbar', FALSE, 'pushtheme'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the theme to use the cover photo supplied with it.'),
    );
    
    $element['topbar']['fix_topbar'] = array(
      '#type' => 'checkbox', 
      '#title' => t('fix the taskbar'), 
      '#default_value' => omega_theme_get_setting('fix_topbar', FALSE, 'pushtheme'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the topbar to stick to the top of the screen.'),
    );

    $element['topbar']['adminonly_taskbar'] = array(
      '#type' => 'checkbox', 
      '#title' => t('adminonly taskbar'), 
      '#default_value' => omega_theme_get_setting('adminonly_topbar', FALSE, 'pushtheme'), 
      '#tree' => FALSE, 
      '#description' => t('Check here if you want the topbar adminonly.'),
    );

// toggle still necessary? or handled by enabling extension?
// checkbox that is enabled if a background is used, a preview of the used image would be appropriate
//  $element['theme_settings']['toggle_topbar']['#type'] = 'checkbox';
//  $element['theme_settings']['toggle_topbar']['#title'] = t('topbar');
//  $element['theme_settings']['toggle_topbar']['#default_value'] = omega_theme_get_setting('toggle_topbar');
  

  return $element;
}
