<?php

/**
 * @file
 * Main extension file for the 'topbar' extension.
 */

/**
 * Implements hook_extension_EXTENSION_registry_alter().
 */
function pushtheme_extension_topbar_theme_registry_alter(&$registry) {
    $registry['html']['includes'][] = drupal_get_path('theme', 'pushtheme') . '/includes/topbar/topbar.inc';
    $registry['html']['preprocess functions'][] = 'pushtheme_extension_topbar_preprocess_page';

}

function pushtheme_extension_topbar_preprocess_page(&$vars) {


$path = drupal_get_path('theme', 'pushtheme');

 // if (omega_theme_get_setting('toggle_topbar')) {
    drupal_add_css($path . '/css/components/topbar.css', array('group' => CSS_THEME, 'weight' => 12, 'every_page' => TRUE));

 // }

}

