<?php

/**
 * @file
 * Main extension file for the 'bgimage' extension.
 */

/**
 * Implements hook_extension_EXTENSION_registry_alter().
 */
function pushtheme_extension_bgimage_theme_registry_alter(&$registry) {


//  if (!empty($registry['html']['images'])) {
    $registry['html']['includes'][] = drupal_get_path('theme', 'pushtheme') . '/includes/bgimage/bgimage.inc';
#    $registry['html']['preprocess functions'][] = 'pushtheme_extension_bgimage_preprocess_html';
     $registry['html']['preprocess functions'][] = 'pushtheme_extension_bgimage_preprocess_page';
//  }
}

function pushtheme_extension_bgimage_preprocess_page(&$vars) {
  $vars['cover_image']  = NULL;
  if (omega_theme_get_setting('toggle_cover_photo')) {
    $cover_image = omega_theme_get_setting('cover_photo_path');

    if (omega_theme_get_setting('default_cover_photo')) {
    // fix for http://drupal.org/node/1026156 http://drupal.org/node/194098 testing http://venutip.com/content/getting-path-your-subtheme
    global $theme_key;
    $path_to_theme = drupal_get_path('theme', $theme_key);
      $cover_image = $path_to_theme . '/background.jpg';

    }
    // TODO: turn this off for admin pages
    if (is_file($cover_image)) {

      $vars['cover_image'] =  file_create_url($cover_image);

    }
    /* Note: This is a quick and dirty way to dynamically change the background cover image.
     * For more control over dynamic CSS, use LESS or SASS, don't extend this.
     */
    $dcss = format_string('html {
      background: url(@background) no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;    
    }', array('@background' => $vars['cover_image']));
    drupal_add_css($dcss, array('type' => 'inline', 'preprocess' => FALSE));
  }

}

#function pushtheme_extension_bgimage_preprocess_html(&$vars) {

# $registry = theme_get_registry();
#if (omega_theme_get_setting('toggle_cover_photo', TRUE)) {
# dpm($registry);
# }
#  $vars['cover_image']  = NULL;
#  // if toggle cover photo checkbox is enabled
#  if (omega_theme_get_setting('toggle_cover_photo')) {
#  // get the path from the theme settings
#    $cover_image = omega_theme_get_setting('cover_photo_path');
#      dpm($cover_image);
#    if (omega_theme_get_setting('default_cover_photo') == TRUE) {
#      $cover_image = path_to_theme() . '/background.jpg';
#    }
#    // TODO: turn this off for admin pages
#    if (is_file($cover_image)) {
#      $vars['cover_image'] =  file_create_url($cover_image);

#    }

#  }

#}

